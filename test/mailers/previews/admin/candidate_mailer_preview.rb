# Preview all emails at http://localhost:3000/rails/mailers/admin/candidate_mailer
class Admin::CandidateMailerPreview < ActionMailer::Preview

  def notify_candidate
    @candidate = Admin::Candidate.last
    Admin::CandidateMailer.notify_candidate(@candidate, "title here")
  end

end
