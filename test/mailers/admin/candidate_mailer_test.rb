require "test_helper"

describe Admin::CandidateMailer do
  let (:candidate) { Admin::Candidate.first }

  it "sends the candidate notification" do
    # skip "This test will intermittantly fail because interview hasn't been wired up to the candidate"
    flunk "Bad Data" unless candidate.interview
    email = Admin::CandidateMailer.notify_candidate(candidate, "title here").deliver
    assert_equal ActionMailer::Base.deliveries.empty?, false
  end
end
