require "test_helper"

describe Location do
  # fixtures :locations

  # minitest spec doesn't like the name Location so use setup instead of let
  #let(:location) { Location.new }
  def setup
    @location = Location.new(locations(:one).attributes)
  end

  it "must be valid" do
    @location.must_be :valid?
  end

  describe "#full_address" do
    it "must return full_address" do
      @location.full_address.must_be :==, "12 Candy Ln, Cityname, ST 12345"
    end
  end
end
