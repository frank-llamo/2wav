require "test_helper"

describe Admin::Interview do
  let(:interview) { admin_interviews(:interview_1) }

  it "must be valid" do
    interview.must_be :valid?
  end

  describe "#interview times" do
    let (:interview) { Admin::Interview.first }

    it "#interview_start" do
      interview.interview_start.wont_be_nil
      interview.interview_start.must_be :==, "9:00AM"
    end

    it "#interview_end" do
      interview.interview_end.wont_be_nil
      interview.interview_end.must_be :==, "12:00PM"
    end
  end
end
