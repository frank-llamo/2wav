require "test_helper"

describe Admin::Candidate do
  let(:candidate_attrs) { Admin::Candidate.new(admin_candidates(:candidate_1).attributes) }

  it "must be valid" do
    candidate_attrs.must_be :valid?
  end

  it "must have a user" do
    candidate = admin_candidates(:candidate_1)
    candidate.must_respond_to :user
    candidate.user.class.must_equal User
  end

  it "publication state must default to in progress" do
    candidate_attrs.publication_state.must_equal "in_progress"
  end

  describe "validations" do
    let(:candidate) { Admin::Candidate.new }

    it "must be invalid without a first name" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a last name" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a phone number" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a greeting" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a title" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a message" do
      candidate.wont_be :valid?
    end
    it "must be invalid without a location" do
      candidate.wont_be :valid?
    end
  end

  describe "#meet_with_interviewers" do
    let (:candidate) { Admin::Candidate.first }

    it "should return the interviewers that will be attending interview_events" do
      x = candidate.meet_with_interviewers
      x.size.must_be :>, 0
    end
  end

  describe "#display_name" do
    let (:candidate) { Admin::Candidate.first }

    it "should return first_name last_name" do
      candidate.display_name.must_be :==, "first_name_1 last_name_1"
    end
  end
end
