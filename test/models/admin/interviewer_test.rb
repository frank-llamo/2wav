require "test_helper"

describe Admin::Interviewer do
  let(:interviewer) { admin_interviewers(:interviewer_1) }

  it "must be valid" do
    interviewer.must_be :valid?
  end

  it "must be invalid" do
    interviewer.first_name = nil
    interviewer.wont_be :valid?
  end

  it "must be invalid" do
    interviewer.last_name = nil
    interviewer.wont_be :valid?
  end

  it "must be invalid" do
    interviewer.title = nil
    interviewer.wont_be :valid?
  end

  it "must be invalid" do
    interviewer.bio = nil
    interviewer.wont_be :valid?
  end
end
