require "test_helper"
require "faker"

describe Admin::UserForCandidate do
  let (:candidate) {
    # Admin::Candidate.new(first_name: Faker::Name.first_name,
    #                      last_name: Faker::Name.last_name,
    #                      phone_number: Faker::PhoneNumber.phone_number,
    #                      greeting: "Hello",
    #                      title: Faker::Name.prefix,
    #                      message: "asdf qwer ty",
    #                      location_id: 1,
    #                      publication_state: 0)
    Admin::Candidate.first
  }

  it "should create a user for the candidate" do
    user_count = User.count
    Admin::UserForCandidate.create_user_for(candidate, Faker::Internet.email)
    User.count.must_be :==, user_count + 1
  end

  it "should associate candidate and user" do
    Admin::UserForCandidate.create_user_for(candidate, Faker::Internet.email)
    user = User.last
    candidate.user.must_be :==, user
  end
end
