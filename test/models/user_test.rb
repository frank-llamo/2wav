require 'test_helper'

describe User do
  # fixtures :users

  def valid_user_params
    {username: "foo", password: "test1234", password_confirmation: "test1234"}
  end
  def valid_extended_params
    {username: "bar", email: "bar@example.com", password: "test1234", password_confirmation: "test1234"}
  end

  describe "fixtures loaded" do
    it "loads admin" do
      assert users(:admin).valid?, "Can't create fixture"
    end
    it "loads user" do
      assert users(:user).valid?, "Can't create fixture"
    end
  end

  describe "is a valid user" do
    it "with valid params" do
      user = User.new valid_user_params
      assert user.valid?, "Can't create with valid params: #{user.errors.messages}"
    end

    it "with valid extended params" do
      user = User.new valid_extended_params
      assert user.valid?, "Can't create with valid extended params: #{user.errors.messages}"
    end

    it "sets default role to user" do
      user = User.new valid_user_params
      assert user.user?, "User role :#{user.role} is wrong"
    end

    it "with :admin role" do
      params = valid_user_params
      params[:role] = :admin
      user = User.new params
      assert user.admin?, "User role :#{user.role} is wrong"
    end
  end

  describe "is an invalid user" do
    it "without either a username or email" do
      params = valid_user_params
      params.delete :username
      user = User.new params
      refute user.valid?, "Can't be valid without username or email"
      assert user.errors[:username], "Missing error when without username or email"
    end

    it "with no role" do
      user = User.new valid_user_params
      user.role = nil
      refute user.valid?, "Missing role"
    end
  end
end
