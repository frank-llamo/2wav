require 'test_helper'

describe ApplicationHelper do
  it "returns US / FR / CN" do
    assert only_rosetta_countries.select {|elem| ["US", "FR", "CN"].include?(elem.first)}.size, 3
  end
end
