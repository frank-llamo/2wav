require "test_helper"

describe Admin::InterviewersController do
  include Devise::TestHelpers

  let (:admin) { users(:admin) }
  def setup
    sign_in admin
  end

  it "should get index" do
    get :index
    assert_response :success
    assigns(:interviewers).wont_be_nil
  end

end
