require "test_helper"

describe Admin::InterviewEventsController do
  include Devise::TestHelpers
  def setup
    sign_in users(:admin)
  end

  it "should get new" do
    get :new
    assert_response :success
  end

end
