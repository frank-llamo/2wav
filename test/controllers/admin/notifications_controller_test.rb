require "test_helper"

describe Admin::NotificationsController do
  include Devise::TestHelpers

  let (:admin) { users(:admin) }
  let (:candidate_tmp) { Admin::Candidate.last }

  def setup
    sign_in admin

    unless candidate_tmp.interview.present?
      skip "Test data didn't load correctly"
    end
  end

  it "should get new" do
    get :new, candidate_id: candidate_tmp.id
    assert_response :success
    assigns(:candidate).wont_be_nil
  end

  it "create should should advance the candidate publication state" do
    assert_equal "in_progress", candidate_tmp.publication_state
    post :create, candidate_id: candidate_tmp.id, notificationtitle: "title here"
    candidate_tmp.reload
    assert_equal "published", candidate_tmp.publication_state
  end
end
