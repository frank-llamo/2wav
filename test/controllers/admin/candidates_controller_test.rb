require "test_helper"

describe Admin::CandidatesController do
  include Devise::TestHelpers

  let (:admin) { users(:admin) }
  let (:contact) { admin_contacts(:one) }

  def setup
    sign_in admin
  end

  it "should get index" do
    get :index
    assert_response :success
    assigns(:candidates).wont_be_nil
  end

  it "should show the candidate" do
    get :show, id: admin_candidates(:candidate_2)
    assert_response :success
    assigns(:candidate).wont_be_nil
  end

  it "should get new" do
    get :new
    assert_response :success
    assigns(:candidate).wont_be_nil
  end

  it "should return search results" do
    c = admin_candidates(:candidate_1)
    search_term = c.first_name[0..2]
    post :search, :format => 'js', search: search_term
    assert_response :success
    assigns(:candidates).wont_be_nil
  end

  describe "#create" do
    def setup
      sign_in admin
      contact_attributes = {first_name: "MyString", last_name: "MyString",
                            job_title: "MyString", email: "MyString",
                            phone_number: "MyString"}

      d = Date.today + 7
      date = "#{d.month}/#{d.day}/#{d.year}"
      start_time = Time.utc(d.year, d.month, d.day, 9, 15)
      end_time = start_time + 15*60 # time increments in seconds
      interview_attributes =  {interview_date: date}
      interview_events_attributes = {event_name: "Arrive", event_begin: start_time, event_end: end_time}
      interview_attributes[:interview_events_attributes] = { "0" => interview_events_attributes }

      @admin_candidate_params = {first_name: "Foo", last_name: "Bar", phone_number: "217-555-1212",
                                 email: "fbar@example.com",
                                 title: "Mr", greeting: "Hi Foo", message: "A message here",
                                 location_id: locations(:one).id,
                                 contact_attributes: contact_attributes,
                                 interview_attributes: interview_attributes}
    end

    it "a new candidate" do
      assert_difference('Admin::Candidate.count') do
        post :create, admin_candidate: @admin_candidate_params
      end
      assigns(:candidate).wont_be_nil
      # assert_redirected_to edit_admin_candidate_path(assigns(:candidate))
      assert_redirected_to admin_candidates_path
    end

    it "a nested contact" do
      # modify this test data so the contact is new
      @admin_candidate_params[:contact_attributes][:email] << "1"
      assert_difference('Admin::Contact.count') do
        post :create, admin_candidate: @admin_candidate_params
      end
    end

    it "a nested interview" do
      assert_difference('Admin::Interview.count') do
        post :create, admin_candidate: @admin_candidate_params
      end
      interview = Admin::Interview.where(candidate: assigns(:candidate)).first
      interview.interview_date.wont_be_nil
    end
  end

  describe "#edit" do
    def setup
      sign_in admin
    end

    it "loads a candidate to edit" do
      get :edit, { id: admin_candidates(:candidate_1).id }
      assert_response :success
      assigns(:candidate).wont_be_nil
    end
  end

  describe "#update" do
    def setup
      sign_in admin
    end

    it "changes the candidate" do
      candidate = admin_candidates(:candidate_1)
      put :update, { id: candidate.id, admin_candidate: {first_name: "Foo"}}
      assert_redirected_to admin_candidates_path
      candidate.reload
      candidate.first_name.must_be :==, "Foo"
    end
  end
end
