require "test_helper"

describe Admin::LocationSitesController do
  include Devise::TestHelpers

  let (:admin) { users(:admin) }
  let(:admin_location_site) { admin_location_sites(:site_location_1) }

  def setup
    sign_in admin
  end

  it "gets index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_location_sites)
  end

  it "gets new" do
    get :new
    assert_response :success
  end

  it "creates admin_location_site" do
    assert_difference('Admin::LocationSite.count') do
      post :create, admin_location_site: { location_id: 1,
                                           site_name: admin_location_site.site_name,
                                           site_url: admin_location_site.site_url }
    end

    assert_redirected_to admin_location_site_path(assigns(:admin_location_site))
  end

  it "shows admin_location_site" do
    get :show, id: admin_location_site
    assert_response :success
  end

  it "gets edit" do
    get :edit, id: admin_location_site
    assert_response :success
  end

  it "updates admin_location_site" do
    put :update, id: admin_location_site, admin_location_site: { location_id: admin_location_site.location_id, site_name: admin_location_site.site_name, site_url: admin_location_site.site_url }
    assert_redirected_to admin_location_site_path(assigns(:admin_location_site))
  end

  it "destroys admin_location_site" do
    assert_difference('Admin::LocationSite.count', -1) do
      delete :destroy, id: admin_location_site
    end

    assert_redirected_to admin_location_sites_path
  end

end
