require "test_helper"

describe Admin::TalentsManagerController do
  include Devise::TestHelpers

  def setup
    @admin = users(:admin)
    sign_in @admin
  end

  it "should get index" do
    get :index
    assert_response :success
  end

end
