# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'faker'

if (Location.count == 0 && Rails.env != "production")
  puts "\nPopulate the locations"
  l = Location.create(city: "Arlington", state: "VA", description: "Suspendisse aliquet massa non lectus molestie venenatis. Aenean elementum tempus risus eget vulputate.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Harrisonburg", state: "VA", description: "Aenean iaculis finibus nibh ac luctus. Integer dapibus, felis eget condimentum molestie, velit orci fringilla mi, sed consequat nulla felis id arcu.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Austin", state: "TX", description: "Aliquam scelerisque tempor turpis non sodales. Nullam elementum feugiat lectus, eget faucibus orci facilisis at.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Boulder", state: "CO", description: "Fusce sed consequat turpis. Nunc bibendum massa quis metus malesuada cursus a congue quam.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Seattle", state: "WA", description: "Nam ultricies, mi ac convallis venenatis, arcu mauris lacinia mauris, nec sollicitudin dolor quam eu nibh. Donec non purus non sem pretium bibendum.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "San Francisco", state: "CA", description: "Nam eleifend dignissim sem lacinia scelerisque. Maecenas euismod quam at mauris blandit aliquam.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Beijing", country: "China", description: "Phasellus vulputate erat sed lacinia tempor. Donec varius tempus erat, eu bibendum purus condimentum vel.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}"

  l = Location.create(city: "Paris", country: "France", description: "Aliquam eu tincidunt urna. Aliquam consequat id mi vel tristique. Curabitur tempor arcu ut libero viverra, sit amet pellentesque arcu dictum.", street_address: Faker::Address.street_address)
  puts "\n  #{l.city}, #{l.state}\n"

  # add 6 dummy sites to each location
  Location.all.each do |l|
    1.upto(6) do |i|
      ls = Admin::LocationSite.create(location_id: l.id, site_name: "Site_#{l.id}_#{i}", site_url: "http://www.site_url_#{l.id}_#{i}.com")
      puts "\n  #{ls.site_name}"
    end
  end
end

# Create developer admin user for testing
unless User.where(username: 'developer').first
  puts "\ncreate user: developer"
  User.create(username: "developer", password: "one2three4!", password_confirmation: "one2three4!", role: :admin)
end

unless User.where(username: 'Admin').first
  puts "\ncreate user: Admin"
  User.create(username: "Admin", password: "admin", password_confirmation: "admin", role: :admin)
end

# Create a candidate user for testing
unless User.where(username: 'candidate').first
  puts "\ncreate user: candidate"

  ActiveRecord::Base.transaction do
    candidate = Admin::Candidate.create(first_name: 'John',
                                        last_name: 'Doe',
                                        phone_number: '555 555-5555',
                                        greeting: "Hello",
                                        title: Faker::Lorem.sentence,
                                        message: Faker::Lorem.paragraph,
                                        location_id: 4)

    candidate.create_user(username: "candidate", password: 'test1234', password_confirmation: 'test1234', email: 'candidate@example.com', role: :user)

    dt = DateTime.now + 7

    candidate.create_interview(interview_date: dt)
    candidate.create_contact(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name,
                             job_title: Faker::Name.title, phone_number: Faker::PhoneNumber.phone_number,
                             email: Faker::Internet.email)

    candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Check In",
                                                                      description: "Meet with the office manager",
                                                                      event_begin: DateTime.new(dt.year, dt.month, dt.day, 9, 45, 0),
                                                                      event_end: DateTime.new(dt.year, dt.month, dt.day, 10, 30, 0))
    candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Meet the team",
                                                                      description: "Meet and greet with the team",
                                                                      event_begin: DateTime.new(dt.year, dt.month, dt.day, 10, 30, 0),
                                                                      event_end: DateTime.new(dt.year, dt.month, dt.day, 11, 00, 0))
    candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Panel interview with",
                                                                      description: "Shay Betterson, John Baxter, Scotty Burnett",
                                                                      event_begin: DateTime.new(dt.year, dt.month, dt.day, 11, 00, 0),
                                                                      event_end: DateTime.new(dt.year, dt.month, dt.day, 12, 00, 0))
    candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Lunch with the team",
                                                                      description: "",
                                                                      event_begin: DateTime.new(dt.year, dt.month, dt.day, 12, 00, 0),
                                                                      event_end: DateTime.new(dt.year, dt.month, dt.day, 13, 00, 0))
    candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Bonnie Hubbard",
                                                                      description: "Recruiter Manager",
                                                                      event_begin: DateTime.new(dt.year, dt.month, dt.day, 13, 00, 0),
                                                                      event_end: DateTime.new(dt.year, dt.month, dt.day, 14, 00, 0))

    candidate.save
  end
end


# Create some candidates in the dev env for dev purposes
if (Admin::Candidate.count <= 1 && Rails.env != "production")
  puts "\ncreate candidates:"
  Faker::Config.locale = 'en-US'
  locations = Location.all

  ActiveRecord::Base.transaction do
    25.times do |i|
      dt = Date.today + i
      first_name = Faker::Name.first_name
      last_name = Faker::Name.last_name
      phone_number = Faker::PhoneNumber.area_code + "-" +
        Faker::PhoneNumber.exchange_code + "-" +
        Faker::PhoneNumber.subscriber_number

      print "  candidate: #{first_name} #{last_name}"
      candidate = Admin::Candidate.create(first_name: first_name,
                                          last_name: last_name,
                                          phone_number: phone_number,
                                          greeting: "Hello #{first_name}",
                                          title: Faker::Lorem.sentence,
                                          message: Faker::Lorem.paragraph,
                                          location_id: locations.sample.id)
      candidate.create_user(username: "c" + i.to_s, password: 'test1234', password_confirmation: 'test1234', email: "#{last_name}@example.com", role: :user)
      print "  username: #{candidate.user.username}"

      candidate.create_interview(interview_date: dt)
      print "  interview date: #{dt}"

      candidate.create_contact(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name,
                               job_title: Faker::Name.title, phone_number: Faker::PhoneNumber.phone_number,
                               email: Faker::Internet.email)
      puts "  contact name: #{candidate.contact.display_name}"

      puts "  create interview events"
      candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Check In",
                                                                        description: "Meet with the office manager",
                                                                        event_begin: DateTime.new(dt.year, dt.month, dt.day, 9, 45, 0),
                                                                        event_end: DateTime.new(dt.year, dt.month, dt.day, 10, 30, 0))
      candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Meet the team",
                                                                        description: "Meet and greet with the team",
                                                                        event_begin: DateTime.new(dt.year, dt.month, dt.day, 10, 30, 0),
                                                                        event_end: DateTime.new(dt.year, dt.month, dt.day, 11, 00, 0))
      candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Panel interview with",
                                                                        description: "Shay Betterson, John Baxter, Scotty Burnett",
                                                                        event_begin: DateTime.new(dt.year, dt.month, dt.day, 11, 00, 0),
                                                                        event_end: DateTime.new(dt.year, dt.month, dt.day, 12, 00, 0))
      candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Lunch with the team",
                                                                        description: "",
                                                                        event_begin: DateTime.new(dt.year, dt.month, dt.day, 12, 00, 0),
                                                                        event_end: DateTime.new(dt.year, dt.month, dt.day, 13, 00, 0))
      candidate.interview.interview_events << Admin::InterviewEvent.new(event_name: "Bonnie Hubbard",
                                                                        description: "Recruiter Manager",
                                                                        event_begin: DateTime.new(dt.year, dt.month, dt.day, 13, 00, 0),
                                                                        event_end: DateTime.new(dt.year, dt.month, dt.day, 14, 00, 0))

      candidate.save
    end
  end
end

if (Admin::Interviewer.count == 0 && Rails.env != "production")
  puts "\ncreate some interviewers"
  10.times do
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name
    title = Faker::Name.title
    bio_url = Faker::Internet.url('linkedin.com')
    bio = Faker::Lorem.paragraph

    puts " interviewer: #{first_name} #{last_name}"
    Admin::Interviewer.create(first_name: first_name, last_name: last_name,
                              title: title, bio_url: bio_url, bio: bio)
  end


  interviewers = Admin::Interviewer.all
  Admin::InterviewEvent.all.each do |ie|
    ie.interviewers << interviewers.sample(2)
  end
end
