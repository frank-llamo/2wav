# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141204174633) do

  create_table "admin_candidates", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.text     "greeting"
    t.string   "title"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "location_id"
    t.integer  "contact_id"
    t.integer  "publication_state", null: false
  end

  add_index "admin_candidates", ["contact_id"], name: "index_admin_candidates_on_contact_id", using: :btree
  add_index "admin_candidates", ["first_name"], name: "index_admin_candidates_on_first_name", using: :btree
  add_index "admin_candidates", ["last_name"], name: "index_admin_candidates_on_last_name", using: :btree
  add_index "admin_candidates", ["location_id"], name: "index_admin_candidates_on_location_id", using: :btree
  add_index "admin_candidates", ["user_id"], name: "index_admin_candidates_on_user_id", using: :btree

  create_table "admin_contacts", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "job_title"
    t.string   "email"
    t.string   "phone_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_contacts", ["email"], name: "index_admin_contacts_on_email", using: :btree
  add_index "admin_contacts", ["last_name"], name: "index_admin_contacts_on_last_name", using: :btree

  create_table "admin_events_interviewers", force: true do |t|
    t.integer  "admin_interview_event_id", null: false
    t.integer  "admin_interviewer_id",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_events_interviewers", ["admin_interview_event_id"], name: "index_admin_events_interviewers_on_admin_interview_event_id", using: :btree
  add_index "admin_events_interviewers", ["admin_interviewer_id"], name: "index_admin_events_interviewers_on_admin_interviewer_id", using: :btree

  create_table "admin_interview_events", force: true do |t|
    t.integer  "admin_interview_id"
    t.string   "event_name"
    t.string   "description"
    t.datetime "event_begin"
    t.datetime "event_end"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "location"
  end

  add_index "admin_interview_events", ["admin_interview_id"], name: "index_admin_interview_events_on_admin_interview_id", using: :btree

  create_table "admin_interviewers", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "title"
    t.string   "bio_url",    limit: 750
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar"
  end

  create_table "admin_interviews", force: true do |t|
    t.date     "interview_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "candidate_id"
  end

  add_index "admin_interviews", ["candidate_id"], name: "index_admin_interviews_on_candidate_id", using: :btree

  create_table "admin_location_sites", force: true do |t|
    t.integer  "location_id"
    t.string   "site_name"
    t.string   "site_url",    limit: 750
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "site_image"
    t.string   "description"
  end

  add_index "admin_location_sites", ["location_id"], name: "index_admin_location_sites_on_location_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.string   "street_address"
    t.string   "zip"
    t.string   "header"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role",                                null: false
    t.string   "username"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
