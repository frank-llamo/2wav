class AddHeaderToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :header, :string
  end
end
