class ChangeAdminInterviewersBioUrlLength < ActiveRecord::Migration
  def change
    change_column :admin_interviewers, :bio_url, :string, limit: 750
  end
end
