class ChangeAdminLocationSitesSiteUrlLength < ActiveRecord::Migration
  def change
    change_column :admin_location_sites, :site_url, :string, limit: 750
  end
end
