class CreateAdminCandidates < ActiveRecord::Migration
  def change
    create_table :admin_candidates do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.text :greeting
      t.string :title
      t.text :message

      t.timestamps
    end

    add_index :admin_candidates, :first_name
    add_index :admin_candidates, :last_name
  end
end
