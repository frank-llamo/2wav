class AddPublicationStateToAdminCandidate < ActiveRecord::Migration
  def change
    add_column :admin_candidates, :publication_state, :integer, null: false
  end
end
