class CreateAdminEventsInterviewers < ActiveRecord::Migration
  def change
    create_table :admin_events_interviewers do |t|
      t.references :admin_interview_event, null: false, index: true
      t.references :admin_interviewer, null: false, index: true

      t.timestamps
    end
  end
end
