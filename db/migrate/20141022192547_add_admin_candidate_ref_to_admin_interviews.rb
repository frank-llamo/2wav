class AddAdminCandidateRefToAdminInterviews < ActiveRecord::Migration
  def change
    add_reference :admin_interviews, :candidate, index: true
  end
end
