class AddLocationToAdminInterviewEvent < ActiveRecord::Migration
  def change
    add_column :admin_interview_events, :location, :string
  end
end
