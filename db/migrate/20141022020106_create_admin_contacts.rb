class CreateAdminContacts < ActiveRecord::Migration
  def change
    create_table :admin_contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :job_title
      t.string :email
      t.string :phone_number

      t.timestamps
    end

    add_index :admin_contacts, :last_name
    add_index :admin_contacts, :email
  end
end
