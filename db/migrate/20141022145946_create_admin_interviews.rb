class CreateAdminInterviews < ActiveRecord::Migration
  def change
    create_table :admin_interviews do |t|
      t.date :interview_date

      t.timestamps
    end
  end
end
