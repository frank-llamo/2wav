class AddDescriptionToAdminLocationSites < ActiveRecord::Migration
  def change
    add_column :admin_location_sites, :description, :string
  end
end
