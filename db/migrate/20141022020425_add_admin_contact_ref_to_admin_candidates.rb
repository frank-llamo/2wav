class AddAdminContactRefToAdminCandidates < ActiveRecord::Migration
  def change
    add_reference :admin_candidates, :contact, index: true
  end
end
