class CreateAdminInterviewers < ActiveRecord::Migration
  def change
    create_table :admin_interviewers do |t|
      t.string :first_name
      t.string :last_name, index: true
      t.string :title
      t.string :bio_url
      t.text :bio

      t.timestamps
    end
  end
end
