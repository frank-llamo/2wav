class AddLocationRefToAdminCandidates < ActiveRecord::Migration
  def change
    add_reference :admin_candidates, :location, index: true
  end
end
