class CreateAdminInterviewEvents < ActiveRecord::Migration
  def change
    create_table :admin_interview_events do |t|
      t.references :admin_interview, index: true
      t.string :event_name
      t.string :description
      t.datetime :event_begin
      t.datetime :event_end

      t.timestamps
    end
  end
end
