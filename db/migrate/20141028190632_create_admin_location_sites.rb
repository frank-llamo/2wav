class CreateAdminLocationSites < ActiveRecord::Migration
  def change
    create_table :admin_location_sites do |t|
      t.references :location, index: true
      t.string :site_name
      t.string :site_url

      t.timestamps
    end
  end
end
