class AddSiteImageToAdminLocationSite < ActiveRecord::Migration
  def change
    add_column :admin_location_sites, :site_image, :string
  end
end
