class AddUserRefToAdminCandidates < ActiveRecord::Migration
  def change
    add_reference :admin_candidates, :user, index: true
  end
end
