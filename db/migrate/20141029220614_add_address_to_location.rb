class AddAddressToLocation < ActiveRecord::Migration
  def change
    add_column :locations, :street_address, :string
    add_column :locations, :zip, :string
  end
end
