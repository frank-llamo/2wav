class AddAvatarToAdminInterviewer < ActiveRecord::Migration
  def change
    add_column :admin_interviewers, :avatar, :string
  end
end
