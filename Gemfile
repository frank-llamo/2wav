source 'https://rubygems.org'

#### Begin Rails generator gems
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.7'
# Use mysql as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
#gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby
gem 'execjs'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
#### End Rails generator gems

### Begin Application specific gems
gem 'dotenv-rails', '~> 1.0.2'
gem 'devise', '~> 3.3.0'
gem 'breadcrumbs_on_rails', '~> 2.3.0' # https://github.com/weppos/breadcrumbs_on_rails
gem 'html5_validators', '~> 1.1.2' # https://github.com/amatsuda/html5_validators
gem 'chosen-rails', '~> 1.2.0'   # https://github.com/tsechingho/chosen-rails
# gem 'jquery-turbolinks' # https://github.com/kossnocorp/jquery.turbolinks fix for document.ready and turbolinks
# gem 'carmen-rails', '~> 1.0.0' # https://github.com/jim/carmen-rails provide nice country/state selects
gem 'mini_magick' # for interfacing with imagemagick file manipulation
gem 'carrierwave', '~> 0.10.0' # https://github.com/carrierwaveuploader/carrierwave for handling file uploads
gem 'ckeditor'
gem 'rails-erd' # gem with graphviz dependency for generating entity relationship diagram
### End Application specific gems

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano', '3.2.1.1', path: 'vendor/gems/capistrano-3.2.1.1'
gem 'capistrano-rbenv', '~> 2.0', group: :development
gem 'capistrano-rails', group: :development
gem 'capistrano-bundler', group: :development
gem 'capistrano-passenger', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
group :development, :test, :staging do
  gem 'awesome_print'
  gem 'hirb'
  gem 'pry-byebug'
  gem 'faker'
end

group :development, :staging do
  gem 'binding_of_caller'
  gem 'better_errors', '~> 2.0.0'
  gem 'quiet_assets'
  gem 'flamegraph'
  gem 'rack-mini-profiler'
  gem 'bullet'
end

group :test do
  gem 'minitest-rails', '~> 2.1.0'
  gem 'minitest-reporters'
end
