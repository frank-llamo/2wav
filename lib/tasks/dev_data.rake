namespace :db do
  desc "Initialize development data"
  task :dev_data do
    puts "drop database"
    Rake::Task["db:drop"].invoke
    puts "create database"
    Rake::Task["db:create"].invoke
    puts "migrate database"
    Rake::Task["db:migrate"].invoke
    puts "seed database"
    Rake::Task["db:seed"].invoke
  end

  desc "Generate ERD"
  task :erd do
    sh "bundle exec erd --attributes=foreign_keys,content"
  end
end
