#Okay, since the Capistrano 3.2.1 svn commands are borked and overriding
#is also borked I scrapped this approach and took a different one.
#Instead of overriding the borked svn commands I copied the
#capistrano-3.2.1 gem into the vendor/gems directory. I incremented the
#version number of the gem to be 3.2.1.1. I changed the version number in
#the config/deploy.rb file. I pointed the Gemfile at the local path. I
#modified the svn:update and svn:set_current_revision tasks so they
#actually work.

## Need to figure out how to actually clear the actions on these 2 tasks to get deployment working.
## Another option would be to create a new Capistrano::Svn::AuthStrategy class derived from Capistrano::Svn::DefaultStrategy
## and override like this.
## namespace :svn do
##   def strategy
##     @strategy ||= Capistrano::Svn.new(self, fetch(:svn_strategy, Capistrano::Svn::AuthStrategy))
##   end
## end
#
##Rake::Task[:'svn:update'].clear_actions
##Rake::Task[:set_current_revision].clear_actions
#namespace :svn do
#  desc 'Pull changes from the remote repo with username and password'
#  task :update => :'svn:clone' do
#    on release_roles :all do
#     # # within repo_path do
#     #   puts '==== begin custom svn update'
#     #   # execute :svn, "update --username='#{fetch(:scm_username)}' --password='#{fetch(:scm_password)}' --no-auth-cache"
#     #   execute :svn, "update --username='#{fetch(:scm_username)}' --password='#{fetch(:scm_password)}' --no-auth-cache"
#     #   puts '==== end custom svn update'
#     # # end
#      strategy.update
#    end
#  end
#
#  desc 'Determine the revision that will be deployed'
#  task :set_current_revision do
#    on release_roles :all do
#      # within repo_path do
#        puts '==== begin custom set_current_revision'
#        # set :current_revision, capture(:svn, "log --username='#{fetch(:scm_username)}' --password='#{fetch(:scm_password)}' --no-auth-cache -l 1 | head -n 2 | tail -n 1 | sed 's/\ *\|.*//'")
#        set :current_revision, capture(:svn, "info #{repo_url} | grep Revision | sed 's/Revision: //'")
#        puts '==== end custom set_current_revision'
#      # end
#    end
#  end
#end
