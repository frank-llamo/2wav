/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
if (typeof(CKEDITOR) !== 'undefined') {
  // CKEDITOR.editorConfig = function(config) {
  //   // config.toolbar = [ [ 'Source', 'Bold' ], ['CreatePlaceholder'] ];
  //   config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Strike,Subscript,Superscript';
  // };

  CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config
    // config.toolbar = [ [ 'Bold', 'Italic' ], ['CreatePlaceholder'] ];

    // The toolbar groups arrangement, optimized for a single toolbar row.
    config.toolbarGroups = [
      { name: 'basicstyles', groups: [ 'basicstyles' ] },
      { name: 'paragraph',   groups: [ 'list', 'indent' ] },
      { name: 'links' },
      { name: 'about' }
    ];

    // The default plugins included in the basic setup define some buttons that
    // are not needed in a basic editor. They are removed here.
    config.removeButtons = 'Anchor,Underline,Strike,Subscript,Superscript';

    // Dialog windows are also simplified.
    config.removeDialogTabs = 'link:advanced';
  };
} else{
  console.log("ckeditor not loaded");
}

