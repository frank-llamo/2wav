// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
  $('.delete_avatar').on('click', function(evt) {
    evt.preventDefault();
    var interviewer_id = $(evt.target).data("id");
    if (confirm("Confirm delete avatar image")) {
      $.post("/admin/interviewers/" + interviewer_id + "/delete_avatar", null, function(data, status, xhr) {
        $('#interviewer_avatar').attr("src", "/assets/blank_profile.jpg");
      }).fail(function(result){
        // alert("failure: " + result.responseText);
      });
    }
    reset_form_element( $('#admin_interviewer_avatar') );
  });

  function reset_form_element (e) {
      e.wrap('<form>').parent('form').trigger('reset');
      e.unwrap();
  }

  $('.remove_avatar').on('click', function(evt) {
    evt.preventDefault();
    reset_form_element( $('#admin_interviewer_avatar') );
  });

  $('.cancel_interviewer_edit').on('click', function(evt) {
    evt.preventDefault();
    window.location = "/admin/interviewers";
  });
});
