// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function() {
  $(".fancyselect").chosen();

  $("#add_interview_event_fields").on('ajax:success', function(evt, data, status, xhr){
    // strip out the form from the incoming data
    var interview_event_fields = data.replace(/<form.*?>/, "");
    interview_event_fields = interview_event_fields.replace(/<\/form>/, "");
    interview_event_fields = interview_event_fields.replace(/<div style="display:none"><input name="utf8" type="hidden" value="&#x2713;" \/><\/div>/, "");
    d = new Date();
    interview_event_fields = interview_event_fields.replace(/_0_/g, '_' + d.getTime().toString() + '_');
    interview_event_fields = interview_event_fields.replace(/\[0\]/g, '[' + d.getTime().toString() + ']');
    $('#interview_events').append(interview_event_fields);

    $(".fancyselect").chosen();

    $('.time-field').timepicker({'scrollDefault': 'now','step':15});

    $('.remove_event').on('click', function(evt) {
      evt.preventDefault();
      $(evt.target).closest('.eventItem').remove();
    });

  });

  // var form = document.getElementById('candidate_form'); // form has to have ID: <form id="formID">
  // form.noValidate = true;
  // form.addEventListener('submit', function(event) { // listen for form submitting
  //   if (!event.target.checkValidity()) {
  //     event.preventDefault(); // dismiss the default functionality
  //     $('[required]').each(function(idx, elem) { if ($(elem).val().length === 0) { $(elem).css('border', 'solid 1px red'); }});
  //   }
  // }, false);


  // This works in desktop Safari but not on the iPad
  // if (!Modernizr.inputtypes.date) { // this is a hacky check for Safari
  //   var form = document.getElementById('candidate_form');
  //   form.noValidate = true;
  //   form.addEventListener('submit', function(event) {
  //     //Prevent submission if checkValidity on the form returns false.
  //     // if (!event.target.checkValidity()) {
  //     var invalid_fields = $('[required]').map(function(idx, elem) { if ($(elem).val().length === 0) { return elem; } });
  //     if (invalid_fields.length > 0) {
  //       event.preventDefault();
  //       //Implement you own means of displaying error messages to the user here.
  //       $('[required]').each(function(idx, elem) { if ($(elem).val().length === 0) { $(elem).css('border', 'solid 1px red'); }});
  //     } else {
  //       $('[required]').each(function(idx, elem) { $(elem).css('border', 'solid 1px red'); });
  //     }
  //   }, false);
  // }
});
