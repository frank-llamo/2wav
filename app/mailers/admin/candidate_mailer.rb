class Admin::CandidateMailer < ActionMailer::Base
  default from: "hr@rosettastone.com"

  def notify_candidate(candidate, title)
    @candidate = candidate
    @title = title
    mail(to: @candidate.email, subject: "Interview with Rosetta Stone")
  end
end
