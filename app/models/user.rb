class User < ActiveRecord::Base
  enum role: [:user, :admin]
  after_initialize :set_default_role, :if => :new_record?

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # NOTE: Not sure I want/need this side of the association yet
  # has_one :candidate, class_name: 'Admin::Candidate'

  validates :username, presence: true
  validates :username, :uniqueness => { :case_sensitive => false }
  # validates :email, :uniqueness => { :case_sensitive => false }
  validates :role, presence: true

  # virtual attribute to allow using either username or email to login
  def login=(login)
    @login = login
  end
  def login
    @login || self.username || self.email
  end

  # Overwrite Devise's email required
  def email_required?
    false
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["username = :value OR email = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end

  private
  def set_default_role
    self.role ||= :user
  end
end
