class Location < ActiveRecord::Base
  has_many :candidates, inverse_of: :location, class_name: "Admin::Candidate"
  has_many :sites, dependent: :destroy, class_name: "Admin::LocationSite"

  accepts_nested_attributes_for :sites

  validates :city, presence: true
  validates :description, presence: true
  validates :street_address, presence: true
  validate  :state_or_country_present

  def display_name
    dn = city + ", "
    dn += state.present? ? state : country
  end

  def full_address
    full_address = street_address + ", " + city + ", "
    full_address +=  state + " " if state
    full_address += zip if zip
  end

  private
  def state_or_country_present
    unless (state.present? || country.present?)
      errors.add(:state, "state or country required") unless state.present?
      errors.add(:country, "") unless country.present?
    end
  end
end
