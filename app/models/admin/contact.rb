class Admin::Contact < ActiveRecord::Base
  has_many :candidates, class_name: "Admin::Candidate"

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :job_title, presence: true
  validates :email, presence: true
  validates :phone_number, presence: true

  def display_name
    first_name + " " + last_name
  end
end
