class Admin::Candidate < ActiveRecord::Base
  enum publication_state: [:in_progress, :published, :on_hold]
  after_initialize :set_publication_state, :if => :new_record?

  belongs_to :user
  belongs_to :location, inverse_of: :candidates
  belongs_to :contact, class_name: "Admin::Contact"
  has_one    :interview, inverse_of: :candidate, class_name: "Admin::Interview"

  accepts_nested_attributes_for :contact
  accepts_nested_attributes_for :interview

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone_number, presence: true
  # validates :greeting, presence: true
  # validates :title, presence: true
  validates :message, presence: {message: "Content for Candidate's page is required"}
  validates :location, presence: true
  validates :publication_state, presence: true
  validate :user_has_email

  def email
    return user.email if user.present?
    return nil
  end
  def email=(email)
    self.user.update!(email: email) if user.present?
  end


  def meet_with_interviewers
    self.interview.interview_events.map {|ie| ie.interviewers.all }.
      flatten.uniq.sort {|a,b| a.last_name <=> b.last_name}
  end

  def display_name
    first_name + " " + last_name
  end


  private
  def set_publication_state
    self.publication_state ||= :in_progress
  end

  def user_has_email
    if user && user.email.blank?
      errors.add(:email, "is required")
    end
  end
end
