class Admin::UserForCandidate
  def self.create_user_for(candidate, email)
    if email.blank?
      candidate.errors.add(:email, "is required")
      raise ApplicationError, "Email is required"
    end
    username = candidate.first_name[0].downcase + candidate.last_name.downcase
    password = password_confirmation = candidate.last_name.downcase

    # If by chance there is already a user with this username tack on 4
    # digits to the end to make it unique
    if User.where(username: username).first
      username += Time.now.to_i.to_s[6..9]
    end

    user = User.new(email: email, password: password, password_confirmation: password, username: username)
    unless user.valid?
      error_message = ""
      user.errors.each do |attribute, message|
        error_message += "  #{attribute} #{message}"
      end
      raise ApplicationError, error_message
    end
    user.save

    candidate.update_attributes(user: user)
  end
end
