class Admin::EventsInterviewers < ActiveRecord::Base
  belongs_to :interview_event, foreign_key: :admin_interview_event_id, class_name: "Admin::InterviewEvent"
  belongs_to :interviewer, foreign_key: :admin_interviewer_id, class_name: "Admin::Interviewer"
end
