class Admin::InterviewEvent < ActiveRecord::Base
  belongs_to :interview, class_name: "Admin::Interview"
  has_many :events_interviewers, foreign_key: :admin_interview_event_id, class_name: "Admin::EventsInterviewers"
  has_many :interviewers, through: :events_interviewers, class_name: "Admin::Interviewer"
end
