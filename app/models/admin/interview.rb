class Admin::Interview < ActiveRecord::Base
  belongs_to :candidate, inverse_of: :interview, class_name: "Admin::Candidate"
  has_many :interview_events, class_name: "Admin::InterviewEvent", foreign_key: "admin_interview_id"

  accepts_nested_attributes_for :interview_events

  # validates :interview_date, presence: true

  def interview_start
    if interview_events.present?
      event_begin = interview_events.order('event_begin').first.event_begin
      return event_begin.strftime("%-l:%M%p") if event_begin.present?
    end
    return ""
  end

  def interview_end
    if interview_events.present?
      event_end = interview_events.order('event_begin').last.event_end
      return event_end.strftime("%-l:%M%p") if event_end.present?
    end
    return ""
  end
end
