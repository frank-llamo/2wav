class Admin::LocationSite < ActiveRecord::Base
  mount_uploader :site_image, SiteImageUploader

  belongs_to :location
end
