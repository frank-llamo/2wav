class Admin::Interviewer < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader

  has_many :events_interviewers, foreign_key: :admin_interviewer_id, class_name: "Admin::EventsInterviewers"
  has_many :interview_events, through: :events_interviewers, class_name: "Admin::InterviewEvent"

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :title, presence: true
  validates :bio, presence: true

  def display_name
    first_name + " " + last_name
  end
end
