# inherit/override the Devise::SessionsController in order to specify the
# layout to use
class SessionsController < Devise::SessionsController
  layout 'login'
end
