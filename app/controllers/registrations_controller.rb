# inherit/override the Devise::RegistrationsController in order to specify the
# layout to use
class RegistrationsController < Devise::RegistrationsController
  layout 'login'
end
