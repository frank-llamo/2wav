class Admin::InterviewEventsController < Admin::AdminController
  def new
    @candidate = Admin::Candidate.new
    @candidate.interview = Admin::Interview.new
    @candidate.interview.interview_events = [Admin::InterviewEvent.new]
    render "new", layout: false
  end
end
