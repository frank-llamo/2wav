class Admin::LocationsController < Admin::AdminController
  before_action :set_admin_location, only: [:show, :edit, :update, :destroy]
  before_action :set_breadcrumbs, except: :index

  def index
    @locations = Location.order('id').all
  end

  def show
  end

  def edit
  end

  def update
    @admin_location.update!(admin_location_params)
    redirect_to admin_locations_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_location
      @admin_location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_location_params
      sites_attributes = [:id, :site_name, :site_url, :site_image, :description]
      params.require(:location).permit(:street_address, :city, :state, :zip, :country, :header, :description, {sites_attributes: sites_attributes})
    end

    def set_breadcrumbs
      super
      self.add_breadcrumb "Locations", :admin_locations_path
    end
end
