class Admin::InterviewersController < Admin::AdminController
  before_action :set_admin_interviewer, only: [:show, :edit, :update]
  before_action :set_breadcrumbs# , except: :show

  def index
    @interviewers = Admin::Interviewer.order('last_name').all
  end

  def show
  end

  def new
    @interviewer = Admin::Interviewer.new
  end

  def create
    @interviewer = Admin::Interviewer.new(interviewer_params)

    respond_to do |format|
      if @interviewer.save
        format.html { redirect_to @interviewer, notice: 'Interviewer was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def update
    if @interviewer.update(interviewer_params)
      redirect_to admin_interviewer_path(@interviewer)
    else
      render "edit"
    end
  end

  def search
    @interviewers = Admin::Interviewer.where('first_name like (:search_term) or last_name like (:search_term)', :search_term => "%#{params[:search]}%")

    respond_to do |format|
      format.js   # just renders interviewers/search.js.erb
    end
  end

  def delete_avatar
    @interviewer = Admin::Interviewer.find(params[:interviewer_id])
    @interviewer.remove_avatar!
    @interviewer.save
    render nothing: true, status: :ok
  end


  private
    def set_admin_interviewer
      @interviewer = Admin::Interviewer.find(params[:id])
    end

    def interviewer_params
      params.require(:admin_interviewer).permit(:first_name, :last_name, :title, :bio, :bio_url, :avatar)
    end

    def set_breadcrumbs
      super
      self.add_breadcrumb "Interviewers", :admin_interviewers_path
    end
end
