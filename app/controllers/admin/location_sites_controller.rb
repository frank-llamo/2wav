class Admin::LocationSitesController < Admin::AdminController
  before_action :set_admin_location_site, only: [:show, :edit, :update, :destroy]

  # GET /admin/location_sites
  # GET /admin/location_sites.json
  def index
    @admin_location_sites = Admin::LocationSite.all
  end

  # GET /admin/location_sites/1
  # GET /admin/location_sites/1.json
  def show
  end

  # GET /admin/location_sites/new
  def new
    @admin_location_site = Admin::LocationSite.new
  end

  # GET /admin/location_sites/1/edit
  def edit
  end

  # POST /admin/location_sites
  # POST /admin/location_sites.json
  def create
    @admin_location_site = Admin::LocationSite.new(admin_location_site_params)

    respond_to do |format|
      if @admin_location_site.save
        format.html { redirect_to @admin_location_site, notice: 'Location site was successfully created.' }
        format.json { render :show, status: :created, location: @admin_location_site }
      else
        format.html { render :new }
        format.json { render json: @admin_location_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/location_sites/1
  # PATCH/PUT /admin/location_sites/1.json
  def update
    respond_to do |format|
      if @admin_location_site.update(admin_location_site_params)
        format.html { redirect_to @admin_location_site, notice: 'Location site was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_location_site }
      else
        format.html { render :edit }
        format.json { render json: @admin_location_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/location_sites/1
  # DELETE /admin/location_sites/1.json
  def destroy
    @admin_location_site.destroy
    respond_to do |format|
      format.html { redirect_to admin_location_sites_url, notice: 'Location site was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_location_site
      @admin_location_site = Admin::LocationSite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_location_site_params
      params.require(:admin_location_site).permit(:location_id, :site_name, :site_url)
    end
end
