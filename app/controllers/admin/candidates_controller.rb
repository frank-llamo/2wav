class Admin::CandidatesController < Admin::AdminController
  before_action :set_breadcrumbs, except: :show

  def index
    @candidates = Admin::Candidate.includes(:interview).order('id')
  end

  def show
    @candidate = Admin::Candidate.includes({interview: :interview_events}).find(params[:id])
    render "/welcome/index", layout: 'application'
  end

  def new
    @candidate = Admin::Candidate.new
    @candidate.contact = Admin::Contact.new
    @candidate.interview = Admin::Interview.new(interview_events: [Admin::InterviewEvent.new])
  end

  def create
    begin
      ActiveRecord::Base.transaction do
        @candidate = Admin::Candidate.new(candidate_params)
        contact_params = candidate_params[:contact_attributes]
        contact = Admin::Contact.where(email: contact_params[:email], last_name: contact_params[:last_name]).first
        unless contact.blank?
          @candidate.contact = contact
          candidate_params.delete(:contact_attributes)
        end

        if @candidate.save
          interview_date = params[:admin_candidate].fetch(:interview_attributes, {}).fetch(:interview_date, nil)
          unless interview_date.blank?
            @candidate.interview.update!(interview_date: Date.strptime(interview_date, "%m/%d/%Y"))
          end

          Admin::UserForCandidate.create_user_for(@candidate, params[:admin_candidate][:email])
          redirect_to admin_candidates_path, notice: "Candidate #{@candidate.first_name} #{@candidate.last_name} created"
        else
          render :new
        end
      end
    rescue ActiveRecord::ActiveRecordError, ApplicationError => error
      flash[:alert] = "Error: #{error.message}"
      render :new
    end
  end

  def edit
    @candidate = Admin::Candidate.includes({interview: {interview_events: :interviewers}}).find(params[:id])
  end

  def update
    @candidate = Admin::Candidate.includes({interview: {interview_events: :interviewers}}).find(params[:id])
    begin
      ActiveRecord::Base.transaction do
        @candidate.update!(candidate_params)

        interview_date = params[:admin_candidate].fetch(:interview_attributes, {}).fetch(:interview_date, nil)
        unless interview_date.blank?
          @candidate.interview.update!(interview_date: Date.strptime(interview_date, "%m/%d/%Y"))
        end
        redirect_to admin_candidates_path, notice: "Candidate #{@candidate.first_name} #{@candidate.last_name} updated"
      end
    rescue ActiveRecord::ActiveRecordError, ApplicationError => error
      flash[:alert] = "Error: #{error.message}"
      render :edit
    end
  end


  def search
    @candidates = Admin::Candidate.where('first_name like (:search_term) or last_name like (:search_term)', :search_term => "%#{params[:search]}%")

    respond_to do |format|
      format.js   # just renders candidates/search.js.erb
    end
  end

  private
    def candidate_params
      contact_attributes = [:id, :first_name, :last_name, :job_title, :email, :phone_number]
      interview_events_attributes = [:id, :event_name, :description, :location, {:interviewer_ids => []}, :event_begin, :event_end]
      interview_attributes = [:id, {interview_events_attributes: interview_events_attributes}]
      params.require(:admin_candidate).permit(:first_name, :last_name, :email, :phone_number,
                                              :greeting, :title, :message, :location_id,
                                              :contact_attributes => contact_attributes,
                                              :interview_attributes => interview_attributes)
    end

    def set_breadcrumbs
      super
      self.add_breadcrumb "Candidate", :admin_candidates_path
    end
end
