class Admin::NotificationsController < Admin::AdminController
  before_action :set_breadcrumbs

  def new
    @candidate = Admin::Candidate.find(params[:candidate_id])
  end

  def create
    @candidate = Admin::Candidate.find(params[:candidate_id])
    @title = params[:notificationtitle]
    if @candidate.interview.interview_events.first.event_name.blank?
      return redirect_to new_admin_candidate_notifications_path(@candidate), alert: "You cannot send an email until there is at least one interview event."
    end

    # For development you can view the html email at this url
    # http://localhost:3000/rails/mailers/admin/candidate_mailer/notify_candidate
    Admin::CandidateMailer.notify_candidate(@candidate, @title).deliver

    # advance the publication state of the candidate
    @candidate.update_attributes(publication_state: :published)

    redirect_to(admin_candidates_path, notice: "Email has been sent to #{@candidate.display_name}")
  end
end
