class Admin::AdminController < ApplicationController
  layout "admin"

  before_action :has_admin_role


  def index
    render nothing: true
  end


  private
  def has_admin_role
    redirect_to root_path, :alert => "Access Denied" unless current_user.admin?
  end

  def set_breadcrumbs
    self.add_breadcrumb "Main", :admin_path
  end
end
