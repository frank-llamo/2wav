class WelcomeController < ApplicationController

  def index
    @candidate = Admin::Candidate.includes({interview: :interview_events}).where(user: current_user).first
    unless @candidate
      sign_out :user
      redirect_to new_user_session_path
    end
  end
end
